import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  getUsuarios() {

    let params = new HttpParams().append('page', '1');
    params = params.append('nombre', 'fabricio');


    return this.http.get('https://reqres.in/api/user', { params }).pipe(
      map((resp: any) => resp['data']))
  }
}
