import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    console.log("paso por el intercertor");
    console.log(req);

    let headers = new HttpHeaders({
      'api-key': 'hafododafsofdosa'
    });

    let reqClone = req.clone({
      headers
    });

    return next.handle(reqClone).pipe(
      catchError(this.manejarError)
    );
  }


  manejarError(error: HttpErrorResponse) {

    console.log("ocurrio un error");
    return throwError(error.message);
  }

  constructor() { }
}
