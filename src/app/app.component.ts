import { Component, OnInit } from '@angular/core';
import { UsuarioService } from './services/usuario.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'interceptores';

  constructor(private usuarioService: UsuarioService) {

  }

  ngOnInit(): void {
    this.cargarData();
  }

  cargarData() {
    this.usuarioService.getUsuarios().subscribe((resp: any) => {
      console.log(resp);
    });
  }
}
